"use client";
import React, { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import { FaArrowDown } from "react-icons/fa";
import { GiPadlock } from "react-icons/gi";
import axios from "axios";
import { useSnackbar } from "@/providers/snack-bar-provider";

type Props = {
  data: any;
};

interface FileWithProgress {
  name: string;
  size?: number;
  progress?: number;
}

export default function UploadFilesScreen(props: Props) {
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone();
  const [filesWithProgress, setFilesWithProgress] = useState<
    FileWithProgress[]
  >(
    props.data.map((fileName: any) => ({
      name: fileName,
      progress: 100,
    }))
  );
  const { showMessage } = useSnackbar();

  useEffect(() => {
    if (acceptedFiles.length > 0) {
      const sendFiles = async () => {
        try {
          const formData = new FormData();

          acceptedFiles.forEach((file) => {
            formData.append("files", file);
          });

          const data = await axios.post(
            "https://back.boostlabs.ai/api/v1/files/uploadMultipleFiles?files",
            formData,
            {
              headers: {
                "Content-Type": "multipart/form-data",
              },
              onUploadProgress: (progressEvent) => {
                const completedPercentage =
                  (progressEvent.loaded / (progressEvent.total ?? 1)) * 100;
                console.log("completedPercentage", completedPercentage);

                setFilesWithProgress((prevFiles) => [
                  ...prevFiles,
                  ...acceptedFiles.map((file) => ({
                    id: file.name,
                    name: file.name,
                    size: file.size,
                    progress: completedPercentage,
                  })),
                ]);

                console.log("filesWithProgress", filesWithProgress); //add the new file + to the progressbar coming from axios
              },
            }
          );
          showMessage({
            severity: "success",
            message: "Arquivo enviado com sucesso!",
            position: { vertical: "bottom", horizontal: "right" },
          });
          console.log("Arquivos enviados com sucesso!", data);
        } catch (error) {
          showMessage({
            severity: "error",
            message: "Erro ao enviar arquivo!",
            position: { vertical: "bottom", horizontal: "right" },
          });
          console.error("Ocorreu um erro ao enviar os arquivos:", error);
        }
      };

      sendFiles();
    }
  }, [acceptedFiles]);

  return (
    <div className="text-white flex m-8 flex-1 ">
      <section
        {...getRootProps({
          className:
            "dropzone container mx-auto p-8 bg-midnightBlue overflow-x-hidden border border-dashed rounded-lg h-full cursor-pointer",
        })}
      >
        <div className="my-6">
          <input {...getInputProps()} />
          <h2 className="font-medium text-xl">
            Solte seus arquivos ou clique para selecionar
          </h2>
        </div>

        <table className="w-full">
          <thead className="bg-green-500 text-white">
            <tr className="border-b border-lightGray">
              <th className="text-left py-3 px-4 uppercase font-semibold text-sm flex items-center gap-3 ">
                Nome <FaArrowDown />
              </th>
              <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                Privado
              </th>
              <th className="text-left py-3 px-4 uppercase font-semibold text-sm hidden lg:table-cell">
                Data
              </th>
              <th className="text-left py-3 px-4 uppercase font-semibold text-sm hidden lg:table-cell">
                Tamanho
              </th>
            </tr>
          </thead>
          <tbody className="text-gray-700">
            {filesWithProgress.map((el, i) => {
              return (
                <tr className="border-b border-lightGray" key={i}>
                  <td className="text-left py-3 px-4">
                    {typeof el === "string" ? el : el.name}
                    {/* type of comming file from get method and from input are different for now */}
                  </td>
                  <td className="text-left py-3 px-4 flex items-center gap-4">
                    <GiPadlock size={24} />
                    <LoadingBar progress={el.progress} />
                  </td>
                  <td className="text-left py-3 px-4 table-cell">
                    {new Date().toLocaleDateString("pt-BR")}
                  </td>
                  <td className="text-left py-3 px-4 table-cell">{el.size}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </div>
  );
}

function LoadingBar({ progress }: { progress?: number }) {
  return (
    <div className="flex w-36 rounded-xl overflow-hidden relative">
      <span className="absolute left-3">
        {progress === 100 ? "Carregado na IA" : "Carregando..."}
      </span>
      <div
        className={`bg-vibrantGreen rounded-l-xl h-6 w-[${progress?.toString()}%] `}
      ></div>
      <div
        className={`bg-white rounded-r-xl h-6 w-[${(
          100 - (progress || 100)
        ).toString()}%]  `}
      ></div>
    </div>
  );
}
