import { ChatListScreen } from "../../../components/dashboard/chat/chat-list";

export default function Home() {
  return (
    <div className="xl:flex flex-1">
      <div className="bg-oceanBlue h-full p-6 xl:flex flex-1 hidden lg:w-full">
        <div className="pt-6 bg-deepBlue    flex-1 flex-col rounded-md flex items-center justify-center">
          <h2 className="text-white font-bold text-xl">
            Selecione um chat e inicie uma nova conversa
          </h2>
        </div>
      </div>

      <div className="xl:hidden h-full ">
        <ChatListScreen />
      </div>
    </div>
  );
}
