import { RenderMenuCards } from "@/components/dashboard/chat/sidebar/render-menu-cards";
import { Button } from "@mui/material";
import { BiCalendar } from "react-icons/bi";
import { FaRegCheckSquare, FaRegCopy } from "react-icons/fa";

const arr = [
  {
    id: "123",
    text: "Cliente entrou em contato as 12:30 informando que estava com dificuldade para usar o ...",
    time: "03/12/23 - 04/12/23",
  },
  {
    id: "1234",
    text: "03/12/23 - 04/12/23",
    time: "Cliente entrou em contato as 12:30 informando que estava com dificuldade para usar o ...",
  },
];

export default function Menu() {
  return (
    <div className="bg-deepBlue p-8 mx-6 mb-6 rounded-b-md">
      <div className="bg-[#D7DDEB] py-4  flex items-center justify-center m-6  flex-col gap-6 rounded-lg">
        <Button className="bg-deepBlue text-lg px-16">Sumarizar</Button>
        <p className="font-medium text-lg px-6">
          O cliente entrou em contato no dia 03/12 as 12:30 informando que
          estava com dificuldade para usar o produto, houve um alerta de
          sentimento negativo, o booster direcionou o atendimento fazendo o
          agendamento de uma reunião onde o problema foi resolvido.
        </p>
        <ul className="text-5xl text-deepBlue flex w-full flex-row-reverse gap-3 px-4">
          <li>
            <BiCalendar />
          </li>
          <li>
            <FaRegCopy size={44} />
          </li>
          <li>
            <FaRegCheckSquare />
          </li>
        </ul>
      </div>
      <RenderMenuCards data={arr} />
    </div>
  );
}
