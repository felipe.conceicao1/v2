"use client";;
import { InputAdornment, TextField } from "@mui/material";
import { FiSearch } from "react-icons/fi";
import { VscCommentUnresolved } from "react-icons/vsc";
import { FaTags } from "react-icons/fa";
import { BiCalendar, BiSmile } from "react-icons/bi";
import { MdAccessTime } from "react-icons/md";
import Link from "next/link";
import { ChatCard } from "./chat-card";

const arr = [
  {
    id: "123",
    img: "/assets/dashboard/random-man.jpg",
    time: "14:35:57",
    alt: "Robson Silva",
    message: "Ok",
    name: "Robson Silva",
    tags: ["Premium", "Ativo"],
    feeling: "good",
  },
  {
    id: "321",
    img: "/assets/dashboard/random-man.jpg",
    time: "14:35:57",
    alt: "Robson Silva",
    message: "Ok",
    name: "Robson Silva",
    tags: ["Premium", "Ativo"],
    feeling: "bad",
  },
];

export function ChatListScreen() {
  return (
    <div className="pt-6 bg-deepBlue h-full xl:flex flex-col border-r-[3px] border-r-[#13131386] flex-1">
      <div className="px-4">
        <TextField
          variant="filled"
          label="Pesquisar"
          type="text"
          className="rounded-lg"
          fullWidth
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <FiSearch className="text-gray-500" />
              </InputAdornment>
            ),
          }}
        />
        <div className="text-white flex text-2xl gap-4 py-4">
          <VscCommentUnresolved />
          <FaTags />
          <BiSmile />
          <MdAccessTime />
          <BiCalendar />
        </div>
      </div>
      {arr.map((el) => {
        return (
          <Link href="/123" key={el.id}>
            <ChatCard
              alt={el.alt}
              feeling={el.feeling}
              image={el.img}
              message={el.message}
              name={el.name}
              tags={el.tags}
              time={el.time}
            />
          </Link>
        );
      })}
    </div>
  );
}
