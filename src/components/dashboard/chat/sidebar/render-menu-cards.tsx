import { MenuCardType } from "@/utils/types";
import { MenuCard } from "./menu-card";

export function RenderMenuCards({ data }: { data: MenuCardType[] }) {
  return (
    <div className="text-white font-medium text-lg">
      {data.map((el) => {
        return (
          <MenuCard key={el.id} text={el.text} time={el.time} id={el.id} />
        );
      })}
    </div>
  );
}
