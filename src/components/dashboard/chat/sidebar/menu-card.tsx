import { MenuCardType } from "@/utils/types";

export function MenuCard(props: MenuCardType) {
  return (
    <div className="border border-oceanBlue rounded-lg p-2">
      <p>
        <time>{props.time}</time>
        <br />
        <br />
        {props.text}
      </p>
    </div>
  );
}
