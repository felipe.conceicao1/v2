"use client";
import Link from "next/link";
import { useRef, useState } from "react";
import AnimateHeight, { Height } from "react-animate-height";
import { BiCalendar } from "react-icons/bi";
import { FaRegUser } from "react-icons/fa";
import { GiHamburgerMenu } from "react-icons/gi";
import { GrDocumentText } from "react-icons/gr";
import {
  IoArrowDownCircle,
  IoArrowUpCircle,
  IoCartOutline,
} from "react-icons/io5";

export function ChatInfo({ children }: { children: React.ReactNode }) {
  const [checked, setChecked] = useState(false);
  const containerRef = useRef<HTMLElement>(null);
  const [height, setHeight] = useState<Height>(0);

  return (
    <div>
      <div className="flex justify-center pb-2">
        <button
          aria-expanded={height !== 0}
          aria-controls="example-panel"
          onClick={() => setHeight(height === 0 ? "auto" : 0)}
        >
          {height === 0 ? (
            <IoArrowUpCircle className="text-5xl text-white" />
          ) : (
            <IoArrowDownCircle className="text-5xl text-white" />
          )}
        </button>
      </div>

      <AnimateHeight duration={500} height={height}>
        <div className="flex pb-2 bg-deepBlue rounded-t-md flex-col p-4 mx-6">
          <nav className="flex gap-4 text-[40px] text-white justify-between px-4">
            <Link href="/123/">
              <GiHamburgerMenu />
            </Link>
            <Link href="/123/documents">
              <GrDocumentText />
            </Link>
            <Link href="/123/calendar">
              <BiCalendar />
            </Link>
            <Link href="/123/profile">
              <FaRegUser />
            </Link>
            <Link href="/123/cart">
              <IoCartOutline size={48} />
            </Link>
          </nav>
        </div>
        {children}
      </AnimateHeight>
    </div>
  );
}
