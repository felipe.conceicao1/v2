import { ChatInput } from "./chat-input";
import {ChatMessage} from '@/components/dashboard/chat/chat-message';

const messages = [
  {
    id: 8,
    message: "Estou com muita dificuldade em entender como o produto funciona!!!",
    date: "20:00",
    isNegative: true,
    user: {
      name: "Ana Souza",
      avatar: "/assets/dashboard/random-man.jpg",
      isSender: false,
      isBooster: false,
    }
  },
  {
    id: 9,
    message: "@Fabio Estevez identifiquei uma msg negativa da cliente. Minhas respostas automáticas estão pausadas!",
    date: "20:00",
    user: {
      name: "Booster",
      avatar: "/assets/dashboard/robot.png",
      isSender: false,
      isBooster: true,
    }
  },
  {
    id: 10,
    message: "Oi Ana disponível para tirar qualquer dúvida que você tenha.",
    date: "20:00",
    user: {
      name: "Fabio Estevez (Vendedor)",
      avatar: "/assets/dashboard/random-man.jpg",
      isSender: true,
      isBooster: false,
    }
  },
]

export function ChatBox() {
  return (
    <div className='flex flex-col justify-between h-full overflow-y-hidden'>
      <div className='chat-container h-full flex-1 overflow-auto'>
        <div className='bg-[#002935] w-full flex flex-col h-full justify-between'>
          <div id='chat-body' className='overflow-auto flex-1'>
            <div className='flex flex-col p-4'>
              {messages.map((message, index) => (
                <ChatMessage key={index} message={message}/>
              ))}
            </div>
          </div>
        </div>
      </div>
      <ChatInput/>
    </div>
  );
}
