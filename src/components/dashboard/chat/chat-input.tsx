import { Input, InputAdornment } from "@mui/material";
import { IoIosAttach } from "react-icons/io";
import { LuSendHorizonal } from "react-icons/lu";
import { MdOutlineCameraAlt } from "react-icons/md";
import { TiMicrophoneOutline } from "react-icons/ti";

export function ChatInput() {
  return (
    <div className="bg-deepBlue flex p-8 rounded-md">
      <img className="mr-4" src="/assets/dashboard/robot.png" alt="" />
      <Input
        className="bg-white w-full p-2 rounded-md"
        startAdornment={
          <InputAdornment position="start">
            <LuSendHorizonal className="text-deepBlue text-4xl" />
          </InputAdornment>
        }
        endAdornment={
          <InputAdornment position="end" className="flex gap-1">
            <button>
              <IoIosAttach className="text-oceanBlue text-4xl" />
            </button>
            <button>
              <MdOutlineCameraAlt className="text-oceanBlue text-4xl" />
            </button>
            <button>
              <TiMicrophoneOutline className="text-oceanBlue text-4xl" />
            </button>
          </InputAdornment>
        }
      />
    </div>
  );
}
