import React from 'react'
import { ChatWithBotSideBarFilter } from './chat-with-bot-sidebar-filter'
import { ChatWithBotSidebarTags } from './chat-with-bot-sidebar-tags'

const ChatWithBotSideBar = () => {
    return (
        <div className="bg-deepBlue flex flex-col py-8 p-0 z-10 h-full w-full border-r-[3px] border-[#13131386]">
            <div className="text-white flex flex-col w-full transition-all duration-200 ease-in-out h-full z-10">
                <ChatWithBotSideBarFilter />
                <ChatWithBotSidebarTags />
            </div>
        </div>
    )
}





export default ChatWithBotSideBar