import React from "react";
import { AiFillTag } from "react-icons/ai";
import { twMerge } from "tailwind-merge";

type Props = {
  children: React.ReactNode;
  onClick?: () => void;
  className?: string;
};

export function Tags({ children, onClick, className }: Props) {
  function getColor() {
    switch (children) {
      case "Premium":
        return "text-neonYellow"
      case "Ativo":
        return "text-vibrantGreen"
      case "Treinamentos":
        return "text-[#FFC107]"
      case "Produtos":
        return "text-[#07FF3E]"
      case "Vendas":
        return "text-[#0711FF]"
      default:
        return "text-[#0059ff]"
    }

  }

  const mergedClassNames = twMerge(
    "flex text-lg items-center gap-1",
    className
  );

  return (
    <div className={mergedClassNames}>
      <AiFillTag className={twMerge("text-xl", getColor())} onClick={onClick} />
      <span className="text-white">{children}</span>
    </div>
  );
}
