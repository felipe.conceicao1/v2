import { useState } from "react";
import { Button, CircularProgress } from "@mui/material";
import { FcGoogle } from "react-icons/fc";
import { User, signInWithPopup } from "firebase/auth";
import { auth, provider } from "../../utils/firebase.config";
import { GoogleAuthProvider } from "firebase/auth/cordova";
import { setCookie } from "cookies-next";
import { useRouter } from "next/navigation";

export function GoogleSigninButton() {
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const handleGoogleButtonClick = (): void => {
    setIsLoading(true);
    signInWithPopup(auth, provider)
      .then((result) => {
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential?.accessToken;
        const user: User | null = result.user;

        if (token && user) {
          setCookie("accessToken", token, {
            path: "/",
            maxAge: 7 * 24 * 60 * 60,
          });
          setCookie("userId", user.uid, {
            path: "/",
            maxAge: 7 * 24 * 60 * 60,
          });
          router.push("/");
        }
      })
      .catch((error) => {
        const errorMessage = error.message;
        console.log("Error logging in: ", errorMessage);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <>
      {isLoading ? (
        <Button variant="outlined">{<CircularProgress size={22} />}</Button>
      ) : (
        <Button
          variant="outlined"
          type="submit"
          sx={{ justifyContent: "center !important" }}
          className="flex flex-1 font-medium normal-case"
          onClick={handleGoogleButtonClick}
        >
          <FcGoogle size={24} />
          <div className="flex flex-1 justify-center">
            <p>Continue com o Google</p>
          </div>
        </Button>
      )}
    </>
  );
}
