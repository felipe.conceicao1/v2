import { z } from "zod";

export const signInSchema = z.object({
  email: z.string().refine((value) => value.trim() !== '', {
    message: "O campo de e-mail é obrigatório.",
  }),
  password: z.string().refine((value) => value.trim() !== '', {
    message: "O campo de senha é obrigatório.",
  }),
});

export type SignInSchema = z.infer<typeof signInSchema>
