export type MenuCardType = {
  id: string;
  time: string;
  text: string;
};
